<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PhoneController extends Controller
{
    public function index(Request $request) {
        if (!$request->phone) {

            return response()->json(['status' => 'false', 'massage' => 'Пустой телефон']);

        } else if ($request->phone == '79255741630') {

            return response()->json(['status' => 'true', 'massage' => '79255741630']);

        } else if ($request->phone == '79265754842') {

            return response()->json(['status' => 'true', 'massage' => '79265754842']);

        };
    }
}
