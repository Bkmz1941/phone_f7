<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function phone(Request $request) {

        $data1[] = [
                [
                    'nubmer' => '140730',
                    'name' => 'Abonement1',
                    'days' => '68',
                ],
                [
                    'nubmer' => '140731',
                    'name' => 'Abonement2',
                    'days' => '31',
                ]
            ];

        $data2[] = [
            [
                'nubmer' => '140830',
                'name' => 'Abonement3',
                'days' => '73',
            ],
            [
                'nubmer' => '140831',
                'name' => 'Abonement4',
                'days' => '8',
            ],
        ];

        if ($request->phone == '79255741630') {
            return response()->json(['phone' => '79255741630', 'status' => true, 'message' => $data1]);
        } else if ($request->phone == '79265754842') {
            return response()->json(['phone' => '79265754842', 'status' => true, 'message' => $data2]);
        } else {
            return response()->json(['status' => false, 'message' => 'This phone is not registered']);
        }

    }
}
