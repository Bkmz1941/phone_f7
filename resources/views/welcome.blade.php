@extends('layout')

    <div class="views" id="home">
        <div class="view view-main">

            <div class="navbar">
                <div class="navbar-inner">
                    <div class="center sliding">Ваши абонементы</div>
                </div>
            </div>

            <div class="pages navbar-through toolbar-through">
                <div data-page="index" class="page">
                    <div class="page-content">
                        <div class="content-block-title">Введите номер телефона</div>
                        <div class="list-block">
                            <ul>
                                <li>
                                    <div class="item-content">
                                        <div class="item-media"><i class="icon f7-icons">phone</i></div>
                                        <div class="item-inner">
                                            <div class="item-title label">Телефон</div>
                                            <div class="item-input">
                                                <input id="phone" type="email" placeholder="79265754842">
                                            </div>
                                        </div>

                                    </div>
                                </li>
                            </ul>
                        </div>
                        <a href="#" onclick="phone();" class="button active">Найти</a>
                        <div class="content-block-title">Номера для проверки</div>
                        <div class="card">
                            <div class="card-content">
                                <div class="card-content-inner">
                                    <p>79255741630</p>
                                    <p>79265754842</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Страница с таблицей -->
    <script type="text/template" id="myPage">

        <div class="navbar">
            <div class="navbar-inner">
                <div class="left">
                    <a href="#" class="back link">
                        <i class="icon icon-back"></i>
                        <span>Назад</span>
                    </a>
                </div>
                <div class="center">Абонементы</div>
            </div>
        </div>

        <div class="page" data-page="my-page">
            <div class="page-content">
                <div id="table_full">
                    <!-- Тут создается таблица -->
                </div>
            </div>
        </div>

    </script>

        <script src="{{ asset('js/custom.js') }}"></script>

        <script src="{{ asset('/js/F7/framework7.js') }}"></script>
        <script src="{{ asset('/js/F7/my-app.js') }}"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>

