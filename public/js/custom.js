/**
 * Created by admin on 14/09/2017.
 */

function phone() {

    var token = $('#token').attr('content');
    $(function () {
        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN': token }
        });
    });

    var phones = document.getElementById('phone');
    var phone_var = phones.value;
    if (phones.value == '') {
        myApp.alert('Введите номер телефона', 'Ошибка');
    } else {
        $.ajax({
            url: "api/v1/phone?phone=" + phone_var,
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            success: function ajax(data) {
                if (data['phone']) {
                    mainView.router.loadContent($('#myPage').html());
                    $('#table_full').append('<div id="phone_table"></div><div class="data-table card"><table id="table_create"><thead class="label-cell" id="phone_table_thead"></thead><tbody id="phone_table_tbody"></tbody> </table></div>');
                    $('#phone_table').append('<div class="content-block-title">Номер ' + data['phone'] + '<div/>');
                    $('#phone_table_thead').append('<tr><th class="label-cell">' + 'Название' + '</th>' + '<th class="numeric-cell">' + 'Номер' + '</th>' + '<th class="numeric-cell">' +  'Дней осталось' + '</th></tr>');
                    data.message.forEach(function (element) {
                        element.forEach(function (el) {
                            $('#phone_table_tbody').append('<tr><td>' + el['name'] + '</td>' + '<td class="numeric-cell">' + el['nubmer'] + '</td>' + '<td class="numeric-cell">' +  el['days'] + '</td></tr>');
                        });
                    });
                } else {
                    myApp.alert('Такого номера нет', 'Ошибка');
                }
            }
        });
    }
}